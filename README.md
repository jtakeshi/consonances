Classification of Consonance in Generalized Tonal Systems, Jonathan S. Takeshita

Included in this repository is the version of my undergraduate thesis accepted for publication, along with the source code mentioned in the paper. 
The paper has been accepted for publication in The Pentagon (http://www.kappamuepsilon.org/pages/a/pentagon.php),
and was published in Vol. 76, No. 1 (http://kappamuepsilon.org/Pentagon/Vol_76_Num_2_Spring_2017.pdf).

Last revised April 25, 2020.
