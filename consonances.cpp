//Jonathan S. Takeshita
//consonances.cpp
//Algorithm to generate consonance tables for generalized pitch systems

//Compiled on Ubuntu 14.04 LTS with g++-6 as:
// g++-6 consonances.cpp -std=c++14 -O3 -Wall -Werror -pedantic -o consonances
//Debug compilation:
// g++-6 consonances.cpp -std=c++14 -g -DDEBUG -Wall -Werror -pedantic -o consonances
//Run as $ ./consonances N

//TODO
//Add LaTex output mode
//Add .tsv or .csv output mode

#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

using namespace std;

//Forward declarations

//Enumerated values representing consonance of pitches
typedef enum {
  NOT_CLASSIFIED, PERFECT, CONSONANT, DISSONANT
} IntervalType;

//Prints out the proper usage of the program
inline void usage();

//Main algorithm, returns a vector with consonances of each interval
vector<IntervalType> consonances(unsigned int n);

//Prints a vector of consonances of intervals
inline void printList(const vector<IntervalType> & vars);

//Returns string corresponding to the appropriate enum
inline string toString(const IntervalType p);

//Generates 8k+4 given k
inline unsigned int numTones(unsigned int k) {
  return (8 * k) + 4;
}

int main(int argc, char ** argv) {
  if ((argc != 2) || (unsigned int) atoi(argv[1]) < 1) {
    usage();
    return 0;
  }

  unsigned int num_tones = numTones(
      (unsigned int) atoi(argv[1]));
  vector<IntervalType> pitch_classifications = consonances(
      num_tones);
  cout << "n = " << num_tones << endl;
  printList(pitch_classifications);
  cout << endl;

}

inline void usage() {
  cout
      << "Call as:\n\t./main N \nN >= 1 is the number of tables to be generated"
      << endl;
  return;
}

vector<IntervalType> consonances(unsigned int n) {

#ifdef DEBUG
  if (n % 8 != 4) {
    cerr << "consonances: invalid argument " << n << endl;
    return vector<IntervalType>(0);
  }
#endif

  vector<IntervalType> vals(n,
      IntervalType(NOT_CLASSIFIED));
  //Initial assignments
  //Unison
  vals[0] = PERFECT;
  //Perfect 5th
  vals[(n / 2) + 1] = PERFECT;
  //Perfect 4th
  vals[(n / 2) - 1] = PERFECT;
  //Aug. 4th
  vals[n / 2] = DISSONANT;
  //Main iteration
  for (unsigned int i = 0; i < n; i++) {
    if (vals[i] == NOT_CLASSIFIED) {
      if (i < (n / 2)) {
        vals[i] =
            ((i % 4 == 1) || (i % 4 == 2)) ?
                DISSONANT : CONSONANT;
      }
      else {
        vals[i] =
            ((i % 4 == 3) || (i % 4 == 2)) ?
                DISSONANT : CONSONANT;
      }
    }
  }

#ifdef DEBUG
  assert(std::find(vals.begin(), vals.end(), NOT_CLASSIFIED) == vals.end());
#endif

  return vals;
}

inline void printList(const vector<IntervalType> & vars) {
  for (unsigned int i = 0; i < vars.size(); i++) {
    cout << i << '\t' << toString(vars[i]) << endl;
  }
  return;
}

inline string toString(const IntervalType p) {
  switch (p) {

  case NOT_CLASSIFIED: {
    cerr << "toString: unclassified pitch encountered";
    return "Unclassified: error in program";
  }

  case PERFECT: {
    return "Perfect";
  }
  case DISSONANT: {
    return "Dissonant";
  }
  case CONSONANT: {
    return "Consonant";
  }

  }
  return "";
}
